package javaoop;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by John on 2016-04-07.
 */
public class ManagerWithFixedSalary extends Employee implements IManager {
    private List<Employee> peopleHired = new ArrayList<Employee>();
    private int salaryCap;
    private int sumOfSalaries = 0;

    public ManagerWithFixedSalary(String name, int salary, int salaryCap){
        super(name, salary);
        this.salaryCap = salaryCap;
    }

    public void hireEmployee(Employee employee){
        peopleHired.add(employee);
        sumOfSalaries += employee.getSalary();
    }

    public boolean canHire(Employee employee){

        return (employee.getSalary() + sumOfSalaries <= salaryCap);
    }

    public List<Employee> peopleHired(){
        return peopleHired;
    }

    @Override
    public String toString(){
        return this.getName();
    }
}
