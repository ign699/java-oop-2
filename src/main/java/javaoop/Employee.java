package javaoop;

/**
 * Created by John on 2016-04-07.
 */

public class Employee {
    private String name;
    private int salary;

    public Employee(String name, int salary){
        this.name = name;
        this.salary = salary;
    }

    public String getName(){
        return name;
    }

    public int getSalary(){
        return salary;
    }

    public boolean isSatisfied(){
        return(salary > 10000);
    }
}
