package javaoop;

import java.util.List;

/**
 * Created by John on 2016-04-09.
 */
interface IManager {
     boolean canHire(Employee employee);
     List<Employee> peopleHired();
     void hireEmployee(Employee employee);
}
