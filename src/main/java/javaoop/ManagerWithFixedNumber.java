package javaoop;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by John on 2016-04-07.
 */
public class ManagerWithFixedNumber extends Employee implements IManager {
    private List<Employee> peopleHired = new ArrayList<Employee>();
    private int numberCap;
    private int numberOfEmployees = 0;

    public ManagerWithFixedNumber(String name, int salary, int numberCap){
        super(name, salary);
        this.numberCap = numberCap;
    }

    public void hireEmployee(Employee employee){
        peopleHired.add(employee);
        numberOfEmployees++;
    }

    public boolean canHire(Employee employee){
        return (numberOfEmployees + 1 <= numberCap);
    }

    public List<Employee> peopleHired(){
        return peopleHired;
    }
    @Override
    public String toString(){
        return this.getName();
    }
}
