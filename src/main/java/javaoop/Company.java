package javaoop;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by John on 2016-04-07.
 */
public class Company {
    private String ceo;
    private List<IManager> managers = new ArrayList<IManager>();
    private Company(){}

    private static final Company company = new Company();

    public static Company getCompany(){
        return company;
    }

    public void hireCeo(String name){
        ceo = name;
    }

    public void addManager(IManager manager)
    {
        managers.add(manager);
    }

    @Override
    public String toString(){
        String toReturn ="";
        toReturn = toReturn + ceo + " - CEO";
        for(IManager manager : managers)
        {
            toReturn = toReturn + "\n\t" + manager.toString() + " - Manager";
            for (Employee employee : manager.peopleHired()){
                toReturn = toReturn + "\n\t\t" + employee.getName() + " - Employee";
            }
        }

        return toReturn;
    }


}
