
import javaoop.Employee;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by John on 2016-04-07.
 */
public class EmployeeTest {
    private Employee employee;
    @Before
    public void setUp() throws Exception{
        employee = new Employee("John", 9000);
    }

    @Test
    public void name() throws Exception {
        assertEquals(employee.getName(), "John");
    }

    @Test
    public void salary() throws Exception {
        assertEquals(employee.getSalary(), 9000);
    }

    @Test
    public void isSatisfied() throws Exception {
        assertEquals(employee.isSatisfied(), false);
    }
}