import javaoop.Company;
import javaoop.Employee;
import javaoop.ManagerWithFixedNumber;
import javaoop.ManagerWithFixedSalary;
import org.junit.Test;


import static org.junit.Assert.*;

/**
 * Created by John on 2016-04-07.
 */
public class CompanyTest {
    private Employee one = new Employee("a", 1);
    private Employee two = new Employee("b", 1);
    private Employee three = new Employee("c", 1);
    private Employee four = new Employee("d", 1);

    ManagerWithFixedSalary uno = new ManagerWithFixedSalary("Pawel",10, 1000);
    ManagerWithFixedNumber dos = new ManagerWithFixedNumber("michal",10, 1000);
    Company company = Company.getCompany();
    @Test
    public void toStringtest() throws Exception {
        company.hireCeo("Mariusz");
        uno.hireEmployee(one);
        uno.hireEmployee(two);
        dos.hireEmployee(three);
        dos.hireEmployee(four);
        company.addManager(uno);
        company.addManager(dos);
        assertEquals("Mariusz - CEO\n\tPawel - Manager\n\t\ta - Employee\n\t\tb - Employee"
                        + "\n\tmichal - Manager\n\t\tc - Employee\n\t\td - Employee", company.toString());
    }
}