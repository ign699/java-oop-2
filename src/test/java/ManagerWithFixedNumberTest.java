import javaoop.ManagerWithFixedNumber;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by John on 2016-04-07.
 */
public class ManagerWithFixedNumberTest {
    ManagerWithFixedNumber managerWithFixedNumber;

    @Before
    public void setUp() throws Exception {
        managerWithFixedNumber = new ManagerWithFixedNumber("john", 9000, 1);
    }
    /*
        Not really sure how to test this method
     */
    @Test
    public void hireEmployee() throws Exception {

    }

    @Test
    public void canHire() throws Exception {
        assertEquals(managerWithFixedNumber.canHire(managerWithFixedNumber), true);
    }
}